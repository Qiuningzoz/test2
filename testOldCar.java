import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class testOldCar {

    //Stop at 3rd speedup
    @Test
    public void testSpeedUp(){
        OldCar c = new OldCar(10);
        c.speedUp(3);
        c.speedUp(2); 
        c.speedUp(5); 
        assertEquals(15,c.getSpeed()); 
    }

    //Speedupnegitive works
    @Test
    public void testnegSpeedUp(){
        OldCar b = new OldCar(20);
        b.speedUp(-2);
        assertEquals(18,b.getSpeed()); 
    }

    //Speedup negetive stop at 3rd times
    @Test
    public void testneg3SpeedUp(){
        OldCar c = new OldCar(40);
        c.speedUp(-2);
        c.speedUp(-3);
        c.speedUp(-4);
        assertEquals(35,c.getSpeed()); 
    }
    
    //Speedup stop at 6th 
    @Test
    public void testsixtimesSpeedUp(){
        OldCar d = new OldCar(10);
        d.speedUp(3);
        d.speedUp(2); 
        d.speedUp(5); 
        d.speedUp(6); 
        d.speedUp(7); 
        d.speedUp(8); 
        assertEquals(28,d.getSpeed()); 
    }

    //speed up at negetive 6th
    @Test
    public void testsixtimesNegSpeedUp(){
        OldCar e = new OldCar(10);
        e.speedUp(-3);
        e.speedUp(-2); 
        e.speedUp(-5); 
        e.speedUp(-6); 
        e.speedUp(-7); 
        e.speedUp(-8); 
        assertEquals(-8,e.getSpeed()); 
    }

    //test stop
    @Test
    public void topSpeedUp(){
        OldCar e = new OldCar(10);
        e.speedUp(-3);
        e.speedUp(-2); 
        e.speedUp(-5); 
        e.speedUp(-6); 
        e.speedUp(-7); 
        e.speedUp(-8); 
        e.stop();
        assertEquals(0,e.getSpeed()); 
    }

    //test reset times
    @Test
    public void toptimesSpeedUp(){
        OldCar e = new OldCar(10);
        e.speedUp(-3);
        e.speedUp(-2); 
        e.speedUp(-5); 
        e.speedUp(-6); 
        e.speedUp(-7); 
        e.speedUp(-8); 
        e.stop();
        assertEquals(0,e.getSpeed()); 
        assertEquals(0,e.getTimes()); 
    }


}
