public class OldCar extends Car{

    private int times;

    public OldCar(int speed){
        super(speed);
        this.times = 1;
    }

    public void speedUp(int tspeed){
        if (this.times % 3 != 0 || this.times == 0){
            this.speed = this.speed + tspeed;
        }
        this.times++;
    }

    public void stop(){
        this.speed = 0;
        this.times = 0;
    }

    public int getTimes(){
        return this.times;
    }
}
