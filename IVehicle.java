public interface IVehicle{
    void speedUp(int speed);
    void stop();
}