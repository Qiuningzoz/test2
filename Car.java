public abstract class Car implements IVehicle{
    protected int speed;

    public Car(int speed){
        this.speed = speed;
    }

    public int getSpeed(){
        return this.speed;
    }

    public String toString(){
        return "The car is moving at speed" + this.speed;
    }

}
